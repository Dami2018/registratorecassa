package org.damiano.crud.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.damiano.crud.models.Constants;
import org.damiano.crud.models.dto.GenericResponseDTO;
import org.damiano.crud.models.dto.ResponseMessageType;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CORSFilter extends GenericFilterBean {

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		System.out.println("\nFiltering on...........................................................");
		HttpServletRequest httpReq = (HttpServletRequest) req;
        if (!isValidApplicationKey(httpReq))
            buildResponseError(res);

		chain.doFilter(req, res);
	}



	private boolean isValidApplicationKey(HttpServletRequest req) {
		String applicationKey = req.getHeader(Constants.APPLICATION_KEY);
        System.out.println("\n"+applicationKey+"?=?"+Constants.APPLICATION_KEY_VALUE);
		if (!Constants.APPLICATION_KEY_VALUE.equals(applicationKey))
			return false;
		return true;
	}

	private void buildResponseError(ServletResponse response) throws ServletException {

		response.setContentType(MediaType.APPLICATION_JSON_VALUE);

		GenericResponseDTO dto = new GenericResponseDTO();
		dto.setData(null);
		dto.setMessage(HttpStatus.BAD_REQUEST.toString());
		dto.setMessageType(ResponseMessageType.ERROR);

		try {

			ObjectMapper objectMapper = new ObjectMapper();

            HttpServletResponse httpResponse = (HttpServletResponse) response;
            objectMapper.writeValue(httpResponse.getOutputStream(), dto);
            httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, "Required headers not specified in the request");

        } catch (Exception e) {
            e.printStackTrace();
		}
	}
}