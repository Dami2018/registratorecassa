package org.damiano.crud.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EntityScan(basePackages = {"org.damiano.crud.models"})
@EnableJpaRepositories(basePackages = "org.damiano.crud.repositories")
@PropertySource(value = {"file:C:\\Users\\Dami\\Desktop\\SpringEs\\apps-configuration\\application.properties"})
public class JpaConfig {
}