package org.damiano.crud.controllers;

import org.damiano.crud.models.dto.ListaProdottiDTO;
import org.damiano.crud.models.dto.PizzeriaInvoiceDTO;
import org.damiano.crud.services.interfaces.ICashRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.math.BigDecimal;

@RestController
@RequestMapping("/registro_cassa")
public class CashRegisterController {

    @Autowired
    private ICashRegisterService cashRegisterService;

    @CrossOrigin
    @PostMapping("/conti")
    public ResponseEntity<BigDecimal> doTotal(@RequestBody ListaProdottiDTO dto){
        ResponseEntity<BigDecimal> entityDTO;
        BigDecimal tot;
        try {
            tot = cashRegisterService.doSum(dto);
            entityDTO = new ResponseEntity<>(tot, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

}
