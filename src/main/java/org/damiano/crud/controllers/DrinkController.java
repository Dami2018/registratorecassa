package org.damiano.crud.controllers;

import org.damiano.crud.models.dto.DrinkDTO;
import org.damiano.crud.services.interfaces.IDrinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/bibite")
public class DrinkController {

    @Autowired
    private IDrinkService drinkService;

    @PostMapping
    public ResponseEntity<DrinkDTO> create(@RequestBody DrinkDTO dto){
        ResponseEntity<DrinkDTO> entityDTO;
        DrinkDTO dto1;
        try {
            dto1 = drinkService.register(dto);
            entityDTO = new ResponseEntity<>(dto1, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @GetMapping("/{id}")
    public ResponseEntity<DrinkDTO> getById(@PathVariable Long id) {
        ResponseEntity<DrinkDTO> entityDTO;
        DrinkDTO dto;
        try {
            dto = drinkService.getProductById(id);
            entityDTO = new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @PutMapping("/{id}")
    public ResponseEntity<DrinkDTO> modify(@RequestBody DrinkDTO dto, @PathVariable Long id) {
        ResponseEntity<DrinkDTO> entityDTO;
        DrinkDTO dto1;
        try {
            dto1 =  drinkService.update(dto, id);
            entityDTO = new ResponseEntity<>(dto1, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> cancel(@PathVariable Long id) {
        ResponseEntity<DrinkDTO> entityDTO;
        try {
            drinkService.delete(id);
            entityDTO = new ResponseEntity<>(HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<DrinkDTO>> getList() {
        ResponseEntity<List<DrinkDTO>> entityDTO;
        List<DrinkDTO> dtos;
        try {
            dtos = drinkService.getAll();
            entityDTO = new ResponseEntity<>(dtos, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }
}
