package org.damiano.crud.controllers;

import org.damiano.crud.models.dto.PizzeriaInvoiceDTO;
import org.damiano.crud.services.interfaces.IPizzeriaInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/invoice")
public class PizzeriaInvoiceController {
    @Autowired
    private IPizzeriaInvoiceService pizzeriaInvoiceService;

    @CrossOrigin
    @PostMapping
    public ResponseEntity<PizzeriaInvoiceDTO> create(@RequestBody PizzeriaInvoiceDTO dto){
        ResponseEntity<PizzeriaInvoiceDTO> entityDTO;
        PizzeriaInvoiceDTO dto1;
        try {
            dto1 = pizzeriaInvoiceService.register(dto);
            entityDTO = new ResponseEntity<>(dto1, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PizzeriaInvoiceDTO> getById(@PathVariable Long id) {
        ResponseEntity<PizzeriaInvoiceDTO> entityDTO;
        PizzeriaInvoiceDTO dto;
        try {
            dto = pizzeriaInvoiceService.getInvoiceById(id);
            entityDTO = new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @PutMapping("/{id}")
    public ResponseEntity<PizzeriaInvoiceDTO> modify(@RequestBody PizzeriaInvoiceDTO dto, @PathVariable Long id) {
        ResponseEntity<PizzeriaInvoiceDTO> entityDTO;
        PizzeriaInvoiceDTO dto1;
        try {
            dto1 =  pizzeriaInvoiceService.update(dto, id);
            entityDTO = new ResponseEntity<>(dto1, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> cancel(@PathVariable Long id) {
        ResponseEntity<PizzeriaInvoiceDTO> entityDTO;
        try {
            pizzeriaInvoiceService.delete(id);
            entityDTO = new ResponseEntity<>(HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @CrossOrigin
    @GetMapping("/last")
    public ResponseEntity<PizzeriaInvoiceDTO> obtainLastInvoice() {
        ResponseEntity<PizzeriaInvoiceDTO> entityDTO;
        PizzeriaInvoiceDTO dto;
        try {
            dto = pizzeriaInvoiceService.getLastInvoice();
            entityDTO = new ResponseEntity<>(dto, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }
}
