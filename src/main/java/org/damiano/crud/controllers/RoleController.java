package org.damiano.crud.controllers;

import org.damiano.crud.models.Role;
import org.damiano.crud.models.dto.RoleDTO;
import org.damiano.crud.repositories.RoleRepository;
import org.damiano.crud.services.GenericController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role")
public class RoleController extends GenericController<Role, RoleDTO, Long> {
    public RoleController(RoleRepository repo) {
        super(repo);
    }
}
