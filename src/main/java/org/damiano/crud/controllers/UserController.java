package org.damiano.crud.controllers;

import org.damiano.crud.models.dto.UserDTO;
import org.damiano.crud.services.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping("/register")
    public ResponseEntity<UserDTO> registration(@RequestBody UserDTO userDTO) {
        ResponseEntity<UserDTO> entityDTO;
        UserDTO userDto;
        try {
            userDto = userService.register(userDTO);
            entityDTO = new ResponseEntity<>(userDto, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> update(@PathVariable Long id, @RequestBody UserDTO userDTO)
    {
        ResponseEntity<UserDTO> entityDTO;
        UserDTO userDto;
        try {
            userDto =  userService.update(userDTO, id);
            entityDTO = new ResponseEntity<>(userDto, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

    @GetMapping("/me")
    public ResponseEntity<UserDTO> getLoggedUser() {
        ResponseEntity<UserDTO> entityDTO;
        UserDTO userDto;
        try {
            UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            userDto =  userService.getUserByUsername(userDetails.getUsername());
            entityDTO = new ResponseEntity<>(userDto, HttpStatus.OK);
        } catch (IOException e) {
            entityDTO = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return entityDTO;
    }

}
