package org.damiano.crud.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true, value = { "hibernateLazyInitializer" })
public class BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Transient
    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (!this.getClass().isAssignableFrom(obj.getClass()))
            return false;
        if (this.getId() == null)
            return false;
        return Objects.equals(this.getId(), ((BaseEntity) obj).getId());
    }

    @Transient
    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
