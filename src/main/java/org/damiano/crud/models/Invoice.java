package org.damiano.crud.models;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;

@MappedSuperclass
public abstract class Invoice extends BaseEntity{

    @Column(name = "nome_azienda")
    private String enterpriseName;

    @Column(name = "indirizzo")
    private String address;

    @Column(name = "nr_tel")
    private String nrTel;

    @Column(name = "cap_città")
    private String capAndCity;

    @Basic
    private String pIva;

    @Basic
    private LocalDateTime date;

    @ElementCollection
    @CollectionTable(
            name="scontrino_prodotti",
            joinColumns=@JoinColumn(name="invoice_id")
    )
    @Column(name = "product")
    private Collection<String> products;

    @Column(name = "totale")
    private BigDecimal tot;


    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNrTel() {
        return nrTel;
    }

    public void setNrTel(String nrTel) {
        this.nrTel = nrTel;
    }

    public String getCapAndCity() {
        return capAndCity;
    }

    public void setCapAndCity(String capAndCity) {
        this.capAndCity = capAndCity;
    }

    public String getpIva() {
        return pIva;
    }

    public void setpIva(String pIva) {
        this.pIva = pIva;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Collection<String> getProducts() {
        return products;
    }

    public void setProducts(Collection<String> products) {
        this.products = products;
    }

    public BigDecimal getTot() {
        return tot;
    }

    public void setTot(BigDecimal tot) {
        this.tot = tot;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "enterpriseName='" + enterpriseName + '\'' +
                ", address='" + address + '\'' +
                ", nrTel='" + nrTel + '\'' +
                ", capAndCity='" + capAndCity + '\'' +
                ", pIva='" + pIva + '\'' +
                ", date=" + date +
                ", products=" + products +
                ", tot=" + tot +
                '}';
    }
}
