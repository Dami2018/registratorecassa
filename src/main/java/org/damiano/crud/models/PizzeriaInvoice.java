package org.damiano.crud.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "scontrini")
public class PizzeriaInvoice extends Invoice {

    @Column(name = "tavolo")
    private String nrTable;

    public PizzeriaInvoice() {
    }

    public String getNrTable() {
        return nrTable;
    }

    public void setNrTable(String nrTable) {
        this.nrTable = nrTable;
    }
}
