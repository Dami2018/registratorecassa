package org.damiano.crud.models;

import javax.persistence.*;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class Product extends BaseEntity{
    @Column(name = "descrizione")
    private String description;

    @Column(name = "prezzo")
    private BigDecimal price;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return this.description+"\t\t"+this.price;
    }
}
