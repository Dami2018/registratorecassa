package org.damiano.crud.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponseDTO<D> {

    private D data;

    private ResponseMessageType messageType;

    private String message;

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public ResponseMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(ResponseMessageType messageType) {
        this.messageType = messageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}