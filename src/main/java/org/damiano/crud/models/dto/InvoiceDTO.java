package org.damiano.crud.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;

public abstract class InvoiceDTO extends DTO{
    @JsonProperty("azienda")
    private String enterpriseName;
    @JsonProperty("via")
    private String address;
    @JsonProperty("tel")
    private String nrTel;
    @JsonProperty("cap_e_città")
    private String capAndCity;
    private String pIva;
    private LocalDateTime date;
    @JsonProperty("prodotti")
    private Collection<String> productDTOS;
    private BigDecimal tot;

    public InvoiceDTO() {
    }

    public InvoiceDTO(String enterpriseName, String address, String nrTel, String capAndCity, String pIva, LocalDateTime date) {
        this.enterpriseName = enterpriseName;
        this.address = address;
        this.nrTel = nrTel;
        this.capAndCity = capAndCity;
        this.pIva = pIva;
        this.date = date;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNrTel() {
        return nrTel;
    }

    public void setNrTel(String nrTel) {
        this.nrTel = nrTel;
    }

    public String getCapAndCity() {
        return capAndCity;
    }

    public void setCapAndCity(String capAndCity) {
        this.capAndCity = capAndCity;
    }

    public String getpIva() {
        return pIva;
    }

    public void setpIva(String pIva) {
        this.pIva = pIva;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Collection<String> getProductDTOS() {
        return productDTOS;
    }

    public void setProductDTOS(Collection<String> productDTOS) {
        this.productDTOS = productDTOS;
    }

    public BigDecimal getTot() {
        return tot;
    }

    public void setTot(BigDecimal tot) {
        this.tot = tot;
    }

    @Override
    public String toString() {
        return "InvoiceDTO{" +
                "enterpriseName='" + enterpriseName + '\'' +
                ", address='" + address + '\'' +
                ", nrTel='" + nrTel + '\'' +
                ", capAndCity='" + capAndCity + '\'' +
                ", pIva='" + pIva + '\'' +
                ", date=" + date +
                ", productDTOS=" + productDTOS +
                ", tot=" + tot +
                '}';
    }
}
