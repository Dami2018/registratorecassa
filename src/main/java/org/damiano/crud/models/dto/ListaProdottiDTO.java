package org.damiano.crud.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ListaProdottiDTO {
    @JsonProperty("lista_prodotti")
    private List<String> products;

    public ListaProdottiDTO() {
    }

    public List<String> getProducts() {
        return products;
    }

    public void setProducts(List<String> products) {
        this.products = products;
    }
}
