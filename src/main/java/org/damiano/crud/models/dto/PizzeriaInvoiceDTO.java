package org.damiano.crud.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class PizzeriaInvoiceDTO extends InvoiceDTO {
    @JsonProperty("nr_tavolo")
    private String nrTable;

    public PizzeriaInvoiceDTO() {
    }

    public PizzeriaInvoiceDTO(String enterpriseName, String address, String nrTel, String capAndCity, String pIva, LocalDateTime date) {
        super(enterpriseName, address, nrTel, capAndCity, pIva, date);
    }

    public String getNrTable() {
        return nrTable;
    }

    public void setNrTable(String nrTable) {
        this.nrTable = nrTable;
    }
}
