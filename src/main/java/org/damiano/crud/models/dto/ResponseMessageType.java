package org.damiano.crud.models.dto;

public enum ResponseMessageType {
    WARNING,
    ERROR,
    INFO,
    AUTH
}