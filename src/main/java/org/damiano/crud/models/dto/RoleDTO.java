package org.damiano.crud.models.dto;

public class RoleDTO extends DTO {
    private String name;
    private String description;

    public RoleDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
