package org.damiano.crud.repositories;

import org.damiano.crud.models.Drink;
import org.damiano.crud.models.dto.DrinkDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface DrinkRepository extends JpaRepository<Drink, Long> {
    @Query("Select d.price from Drink d where d.description = :description")
    BigDecimal getPriceFromDescription(@Param("description") String description);

    @Query("Select d from Drink d where d.description = :description")
    Drink findProductByDescription(@Param("description") String description);
}
