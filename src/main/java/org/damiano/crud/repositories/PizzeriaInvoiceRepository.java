package org.damiano.crud.repositories;

import org.damiano.crud.models.PizzeriaInvoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
public interface PizzeriaInvoiceRepository extends JpaRepository<PizzeriaInvoice, Long> {
    @Query("SELECT pi FROM PizzeriaInvoice pi WHERE pi.id = (SELECT MAX(pi.id) FROM pi)")
    PizzeriaInvoice printLastInvoie();
}
