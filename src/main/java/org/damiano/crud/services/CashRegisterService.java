package org.damiano.crud.services;

import org.damiano.crud.models.dto.ListaProdottiDTO;
import org.damiano.crud.models.dto.PizzeriaInvoiceDTO;
import org.damiano.crud.repositories.DrinkRepository;
import org.damiano.crud.services.interfaces.ICashRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CashRegisterService implements ICashRegisterService {

    @Autowired
    private DrinkRepository drinkRepository;

    @Autowired
    private PizzeriaInvoiceService pizzeriaInvoiceService;

    @Override
    public BigDecimal doSum(ListaProdottiDTO dto) throws IOException {
        PizzeriaInvoiceDTO pizzeriaInvoiceDTO =
            new PizzeriaInvoiceDTO("PIZZERIA ROMA", "Via G. Marconi 11",
            "045.8069182", "00169 Roma", "02382900237", LocalDateTime.now());
        pizzeriaInvoiceDTO.setProductDTOS(dto.getProducts());

        List<BigDecimal> prices = new ArrayList<>();

        for (String descriptionProd: pizzeriaInvoiceDTO.getProductDTOS())
            if (!descriptionProd.equals("undefined"))
                prices.add(drinkRepository.getPriceFromDescription(descriptionProd));

        pizzeriaInvoiceDTO.setTot( sum(prices) );

        System.out.println("AOOOO"+pizzeriaInvoiceDTO);

        return pizzeriaInvoiceService.register(pizzeriaInvoiceDTO).getTot();
    }

    private BigDecimal sum(List<BigDecimal> productsPrices){
        BigDecimal s = BigDecimal.ZERO;
        for (BigDecimal prodPrice : productsPrices){
            s = s.add(prodPrice);
        }
        return s;
    }
}

//CONTABILITA' MENSILE