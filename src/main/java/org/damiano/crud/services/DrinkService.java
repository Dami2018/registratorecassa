package org.damiano.crud.services;

import org.damiano.crud.models.Drink;
import org.damiano.crud.models.dto.DrinkDTO;
import org.damiano.crud.repositories.DrinkRepository;
import org.damiano.crud.services.interfaces.IDrinkService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class DrinkService implements IDrinkService {

    @Autowired
    private DrinkRepository drinkRepository;

    @Override
    @Transactional
    public DrinkDTO register(DrinkDTO dto) throws IOException {
        Drink drink = drinkRepository.save(drinkDTOToDrink(dto));
        return drinkToDrinkDTO(drink);
    }

    @Override
    @Transactional
    public DrinkDTO getProductById(Long id) throws IOException {
        Drink drink = drinkRepository.getOne(id);
        return drinkToDrinkDTO(drink);
    }

    @Override
    @Transactional
    public DrinkDTO update(DrinkDTO dto, Long id) throws IOException {
        Drink drink = drinkRepository.getOne(id);
        BeanUtils.copyProperties(dto, drink);
        Drink savedProduct = drinkRepository.save(drink);
        return drinkToDrinkDTO(savedProduct);
    }

    @Override
    @Transactional
    public void delete(Long id) throws IOException {
        drinkRepository.deleteById(id);
    }

    @Override
    @Transactional
    public List<DrinkDTO> getAll() throws IOException {
        List<DrinkDTO> drinks = new ArrayList<>();
        for(Drink drink: drinkRepository.findAll())
        {
            drinks.add(drinkToDrinkDTO(drink));
        }
        return drinks;
    }

    private Drink drinkDTOToDrink(DrinkDTO dto){
        Drink drink = new Drink();
        BeanUtils.copyProperties(dto, drink);
        return drink;
    }

    protected static DrinkDTO drinkToDrinkDTO(Drink drink) {
        DrinkDTO dto = new DrinkDTO();
        BeanUtils.copyProperties(drink, dto);
        return dto;
    }
}