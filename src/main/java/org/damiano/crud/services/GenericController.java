package org.damiano.crud.services;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericController<BEAN, DTO, ID extends Serializable> {

    private Logger logger = LoggerFactory.getLogger(GenericController.class);

    private CrudRepository<BEAN, ID> repo;

    public GenericController(CrudRepository<BEAN, ID> repo) {
        this.repo = repo;
    }

    /**
     * Ottengo il tipo di oggetto attuale
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private BEAN getBean() throws IllegalAccessException, InstantiationException {
        ParameterizedType ptype = (ParameterizedType)this.getClass().getGenericSuperclass();
        Type type = ptype.getActualTypeArguments()[0];
        Class clazz = (Class)type;
        return (BEAN)clazz.newInstance();
    }

    private DTO getDTO() throws IllegalAccessException, InstantiationException {
        return (DTO)((Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[1]).newInstance();
    }

    @GetMapping
    @ResponseBody
    public List<DTO> listAll() throws InstantiationException, IllegalAccessException {
        List<DTO> dtos = new ArrayList<>();
        Iterable<BEAN> all = this.repo.findAll();
        for(BEAN bean: all) {
            DTO dto = getDTO();
            BeanUtils.copyProperties(bean, dto);
            dtos.add(dto);
        }
        return Lists.newArrayList(dtos);
    }

    @PostMapping
    @ResponseBody
    public DTO create(@RequestBody DTO json) throws InstantiationException, IllegalAccessException {

        BEAN bean = getBean();
        BeanUtils.copyProperties(json, bean);
        BEAN created = this.repo.save(bean);

        DTO dto = getDTO();
        BeanUtils.copyProperties(created, dto);

        return dto;
    }

    @GetMapping(value="/{id}")
    @ResponseBody
    public DTO get(@PathVariable ID id) throws InstantiationException, IllegalAccessException {
        BEAN bean = this.repo.findById(id).get();
        DTO dto = getDTO();
        BeanUtils.copyProperties(bean, dto);
        return dto;
    }

    @PutMapping(value="/{id}")
    @ResponseBody
    public DTO update(@PathVariable ID id, @RequestBody DTO json) throws InstantiationException, IllegalAccessException {
        BEAN bean = getBean();
        BeanUtils.copyProperties(json, bean);
        BEAN entity = this.repo.findById(id).get();
        try {
            BeanUtils.copyProperties(entity, json);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }

        DTO dto = null;
        bean = this.repo.save(entity);
        BeanUtils.copyProperties(bean, dto);
        DTO updated = dto;
        return updated;
    }

    @DeleteMapping(value="/{id}")
    @ResponseBody
    public void delete(@PathVariable ID id) {
        this.repo.deleteById(id);
        Maps.newHashMap();
    }
}