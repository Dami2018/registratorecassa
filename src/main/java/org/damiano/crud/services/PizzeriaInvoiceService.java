package org.damiano.crud.services;

import org.damiano.crud.models.PizzeriaInvoice;
import org.damiano.crud.models.dto.PizzeriaInvoiceDTO;
import org.damiano.crud.repositories.PizzeriaInvoiceRepository;
import org.damiano.crud.services.interfaces.IPizzeriaInvoiceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

@Service
public class PizzeriaInvoiceService implements IPizzeriaInvoiceService {
    @Autowired
    private PizzeriaInvoiceRepository pizzeriaInvoiceRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public PizzeriaInvoiceDTO register(PizzeriaInvoiceDTO dto) throws IOException {
        System.out.println("REGISTER lista prodotti: "+dto.getProductDTOS());
        PizzeriaInvoice pizzeriaInvoiceSaved = pizzeriaInvoiceRepository
                .save(pIDTOToPizzeriaInvoice(dto));
        return pizzeriaInvoiceToPIDTO(pizzeriaInvoiceSaved);
    }

    @Override
    public PizzeriaInvoiceDTO getInvoiceById(Long id) throws IOException {
        return pizzeriaInvoiceToPIDTO(pizzeriaInvoiceRepository.getOne(id));
    }

    @Override
    public PizzeriaInvoiceDTO update(PizzeriaInvoiceDTO dto, Long id) throws IOException {
        PizzeriaInvoice pizzeriaInvoice = pizzeriaInvoiceRepository.getOne(id);
        pizzeriaInvoice.setProducts(new HashSet<>());
        BeanUtils.copyProperties(dto, pizzeriaInvoice);
        BeanUtils.copyProperties(dto.getProductDTOS(), pizzeriaInvoice.getProducts());
        return pizzeriaInvoiceToPIDTO(pizzeriaInvoiceRepository.save(pizzeriaInvoice));
    }

    @Override
    public void delete(Long id) throws IOException {
        pizzeriaInvoiceRepository.deleteById(id);
    }

    @Override
    @Scheduled(cron = "0 59 23 * * ?")
    public void dropTable() throws IOException {
        String sql = "TRUNCATE scontrini;";
        entityManager.createNativeQuery(sql);
    }

    @Override
    @Transactional
    public PizzeriaInvoiceDTO getLastInvoice() throws IOException {
        return pizzeriaInvoiceToPIDTO(pizzeriaInvoiceRepository.printLastInvoie());
    }

    private PizzeriaInvoiceDTO pizzeriaInvoiceToPIDTO(PizzeriaInvoice pizzeriaInvoice){
        PizzeriaInvoiceDTO pizzeriaInvoiceDTO = new PizzeriaInvoiceDTO();
        pizzeriaInvoiceDTO.setProductDTOS(new ArrayList<>());
        BeanUtils.copyProperties(pizzeriaInvoice, pizzeriaInvoiceDTO);
        BeanUtils.copyProperties(pizzeriaInvoice.getProducts(), pizzeriaInvoiceDTO.getProductDTOS());

        return pizzeriaInvoiceDTO;
    }
    private PizzeriaInvoice pIDTOToPizzeriaInvoice(PizzeriaInvoiceDTO pizzeriaInvoiceDTO){
        PizzeriaInvoice pizzeriaInvoice = new PizzeriaInvoice();
        pizzeriaInvoice.setProducts(new ArrayList<>());
        BeanUtils.copyProperties(pizzeriaInvoiceDTO, pizzeriaInvoice);
        BeanUtils.copyProperties(pizzeriaInvoiceDTO.getProductDTOS(), pizzeriaInvoice.getProducts());
        return pizzeriaInvoice;
    }
}
