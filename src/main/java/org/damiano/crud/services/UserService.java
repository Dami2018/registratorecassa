package org.damiano.crud.services;

import org.damiano.crud.models.Role;
import org.damiano.crud.models.User;
import org.damiano.crud.models.dto.UserDTO;
import org.damiano.crud.repositories.RoleRepository;
import org.damiano.crud.repositories.UserRepository;
import org.damiano.crud.services.interfaces.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Service
public class UserService implements IUserService, UserDetailsService {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        });
        return authorities;
    }


    @Override
    @Transactional
    public UserDTO getUserByUsername(String username) throws IOException {
        User user = userRepository.findByUsername(username);
        UserDTO userDto = new UserDTO();
        BeanUtils.copyProperties(user, userDto);
        return userDto;
    }

    @Override
    @Transactional
    public UserDTO register(UserDTO userDTO) throws IOException {
        User user = new User();
        BeanUtils.copyProperties(userDTO, user);
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.getOne(1L));
        user.setRoles(roles);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        User userRegistered = userRepository.save(user);
        return getUserById(userRegistered.getId());
    }

    private UserDTO getUserById(Long id) {
        User user =  userRepository.getOne(id);
        UserDTO dto = new UserDTO();
        BeanUtils.copyProperties(user, dto);
        return dto;
    }

    @Override
    @Transactional
    public UserDTO update(UserDTO userDTO, Long id) throws IOException {
        User user = userRepository.getOne(id);
        BeanUtils.copyProperties(userDTO, user);
        User userRegistered = userRepository.save(user);
        return getUserById(userRegistered.getId());
    }

}
