package org.damiano.crud.services.interfaces;

import org.damiano.crud.models.dto.ListaProdottiDTO;
import org.damiano.crud.models.dto.PizzeriaInvoiceDTO;

import java.io.IOException;
import java.math.BigDecimal;

public interface ICashRegisterService {
    BigDecimal doSum(ListaProdottiDTO dto) throws IOException;

}
