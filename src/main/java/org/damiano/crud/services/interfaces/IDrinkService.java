package org.damiano.crud.services.interfaces;

import org.damiano.crud.models.dto.DrinkDTO;
import java.io.IOException;
import java.util.List;

public interface IDrinkService {
    DrinkDTO register(DrinkDTO dto) throws IOException;
    DrinkDTO getProductById(Long id) throws IOException;
    DrinkDTO update(DrinkDTO dto, Long id) throws IOException;
    void delete(Long id) throws IOException;
    List<DrinkDTO> getAll() throws IOException;
}
