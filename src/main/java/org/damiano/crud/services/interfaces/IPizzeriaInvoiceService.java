package org.damiano.crud.services.interfaces;

import org.damiano.crud.models.dto.PizzeriaInvoiceDTO;

import java.io.IOException;

public interface IPizzeriaInvoiceService {
    PizzeriaInvoiceDTO register(PizzeriaInvoiceDTO dto) throws IOException;
    PizzeriaInvoiceDTO getInvoiceById(Long id) throws IOException;
    PizzeriaInvoiceDTO update(PizzeriaInvoiceDTO dto, Long id) throws IOException;
    void delete(Long id) throws IOException;
    void dropTable() throws IOException;
    PizzeriaInvoiceDTO getLastInvoice() throws IOException;
}
