package org.damiano.crud.services.interfaces;

import org.damiano.crud.models.dto.UserDTO;
import java.io.IOException;

public interface IUserService
{
    UserDTO register(UserDTO userDTO) throws IOException;
    UserDTO update(UserDTO userDTO, Long id) throws IOException;
    UserDTO getUserByUsername(String username) throws IOException;
}
