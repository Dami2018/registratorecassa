package org.damiano.crud.utils;

public enum FoodCategories {
    FIRST_DISHES,
    SECOND_PLATE,
    DESSERTS,
    DRINKS;

    FoodCategories() {
    }
}
