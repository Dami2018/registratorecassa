$(document).ready(function(){
    generateMenu();
    generateIvoice();
    doTotal();
    printInvoice();
});

function generateMenu(){
    $.ajax({
        type:"GET",
        url:"http://localhost:8080/bibite",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function(request) {
            request.setRequestHeader("api-key", "QW5hbENsZXRvMSU=");
        },
        }).then(function(risposta){
            for(i=0; i<risposta.length; i++){
                if(i == (risposta.length/2)){
                    $('.myClass').prepend("<br><br><br><br><br><br>");}
                $('.myClass').prepend( " <div class='button' data-quantity='"+risposta[i].prezzo+"'>"+risposta[i].descrizione+"</div>" );
            }
        });
}

array = new Array();
function generateIvoice(){
    var i=0;

    $('.button').on('click', function(){
        $("#prodotti").append("<br><input type='text' style='border:none;' readonly id=\"prodotto"+i+"\">");
        $("#prodotto"+(i++)).val(i+". "+$(this).text()+" "+$(this).data("quantity")+"€");
        array[i] = $(this).text();
    });
}

function doTotal(){
    $('#totButton').on('click', function(){
        $('#totButton').hide();
        $('#stampaButton').show();
        var lista = createInvoice(array);
        alert(lista);
        $.ajax({
            type:"POST",
            url:"http://localhost:8080/registro_cassa/conti",
            contentType: "application/json; charset=utf-8",
            data: lista,
            dataType: "json",
            beforeSend: function(request) {
            request.setRequestHeader("api-key", "QW5hbENsZXRvMSU=");
            },
        }).then(function(risposta){
            $('#totale').val(risposta+"€");
        });
    });
}

function printInvoice(){
    $('#stampaButton').on('click', function(){
        $.ajax({
            type:"GET",
            url:"http://localhost:8080/invoice/last",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            beforeSend: function(request) {
            request.setRequestHeader("api-key", "QW5hbENsZXRvMSU=");
            },
        }).then(function(risposta){
            $('#totalDiv').append(risposta);
        });
    });
}

function createInvoice(a){
    var invoice = '{'+'"lista_prodotti": [';

    for(var i = 1;i<a.length;i++){
        if(i==(a.length-1))
            invoice = invoice+'"'+a[i]+'"]'+'}';
        else
            invoice = invoice+'"'+a[i]+'",';
    }
    return invoice;
}